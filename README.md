## MPSYT installation

The original repo `mps-youtube` seems to not to be well maintained anymore.

Here is the [link](https://github.com/mps-youtube/yewtube) for `yewtube`-
well maintained and good working fork.

You can use `yt-tlp` instead of `youtube-dl`. Well maintained as well, easy to install.
And it's enough to have only one of them to make `yewtube` work.

Here is the link for `yt-dlp` [github](https://github.com/yt-dlp/yt-dlp).
